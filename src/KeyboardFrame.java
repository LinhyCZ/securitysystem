import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;

/***
 * Třída klávesnice, zajišťuje veškerý běh klávesnice
 */
public class KeyboardFrame extends JFrame {
    private final Timer timer = new Timer(1000, e -> handleAction(Action.TIMESECOND));
    private final Timer alarmBlinkTimer = new Timer(500, e -> handleAlarmAction());

    private final int unlockCode = 123;
    private final int alarmTimeLimit = 30;
    private int keyDisabledTimer = 0;

    private int insertedCode = 0;
    private State currentState = State.OFF;

    private boolean doors = false;
    private boolean windows = false;
    private boolean movement = false;

    private int move = 0;
    private boolean alarm = false;
    private int time = 0;
    private int wrong = 0;

    private JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, bZamknout, bDvere, bOkna, bPohyb;
    private JPanel alarmPanel2, alarmPanel1;

    private final static JTextPane pane = new JTextPane();
    private final static JTextPane alarmText1 = new JTextPane();
    private final static JTextPane alarmText2 = new JTextPane();

    static {
        pane.setBounds(-1, 80, 350 , 70);
        pane.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
        pane.setText("Stav: Odkódováno");
        pane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        StyledDocument doc = pane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        alarmText1.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
        alarmText1.setText(" ALARM");
        alarmText1.setBackground(Color.RED);
        StyledDocument docAlarm1 = pane.getStyledDocument();
        docAlarm1.setParagraphAttributes(0, doc.getLength(), center, false);

        alarmText2.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
        alarmText2.setText(" ALARM");
        alarmText2.setBackground(Color.RED);
        StyledDocument docAlarm2 = pane.getStyledDocument();
        docAlarm2.setParagraphAttributes(0, doc.getLength(), center, false);

        pane.setEditable(false);
    }

    /**
     * Konstruktor klávesnice, vytvoří layout klávesnice a nastaví event listenery
     */
    public KeyboardFrame() {
        this.setLayout(null);
        this.setVisible(true);
        this.setSize(350,530);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        JPanel completeKeyboardPanel = new JPanel();
        completeKeyboardPanel.setLayout(new BorderLayout(0, 10));
        completeKeyboardPanel.setBounds(40, 160, 250,320);

        bZamknout=new JButton("Zamknout");
        bZamknout.addActionListener(this::lockButtonHandler);
        completeKeyboardPanel.add(getKeyboardPanel(), BorderLayout.CENTER);
        completeKeyboardPanel.add(bZamknout, BorderLayout.SOUTH);
        completeKeyboardPanel.setBackground(Color.WHITE);


        JPanel completeLayout = new JPanel();
        completeLayout.setLayout(null);
        completeLayout.setSize(350,500);;

        /*DefaultCaret caret = (DefaultCaret) textArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);*/

        bDvere = new JButton("Otevřít dveře");
        bDvere.setBounds(10,0, 310, 25);
        bDvere.addActionListener(this::dvereButtonHandler);

        bOkna = new JButton("Otevřít okno");
        bOkna.setBounds(10,25, 310, 25);
        bOkna.addActionListener(this::oknoButtonHandler);

        bPohyb = new JButton("Pohyb start");
        bPohyb.setBounds(10,50, 310, 25);
        bPohyb.addActionListener(this::movementButtonHandler);

        completeLayout.add(pane);
        completeLayout.add(completeKeyboardPanel);
        completeLayout.add(bDvere);
        completeLayout.add(bOkna);
        completeLayout.add(bPohyb);
        completeLayout.setBackground(Color.WHITE);

        this.add(completeLayout);
    }

    /**
     * Vytvoří tlačítka klávesnice
     * @return JPanel s tlačítky
     */
    private JPanel getKeyboardPanel() {
        JPanel keyboardPanel = new JPanel();
        keyboardPanel.setBackground(Color.WHITE);

        GridLayout keyboardLayout = new GridLayout(4, 3);
        keyboardLayout.setHgap(5);
        keyboardLayout.setVgap(5);



        keyboardPanel.setLayout(keyboardLayout);
        b1=new JButton("1");
        b2=new JButton("2");
        b3=new JButton("3");
        b4=new JButton("4");
        b5=new JButton("5");
        b6=new JButton("6");
        b7=new JButton("7");
        b8=new JButton("8");
        b9=new JButton("9");
        b0=new JButton("0");

        b1.addActionListener(this::numberButtonHandler);
        b2.addActionListener(this::numberButtonHandler);
        b3.addActionListener(this::numberButtonHandler);
        b4.addActionListener(this::numberButtonHandler);
        b5.addActionListener(this::numberButtonHandler);
        b6.addActionListener(this::numberButtonHandler);
        b7.addActionListener(this::numberButtonHandler);
        b8.addActionListener(this::numberButtonHandler);
        b9.addActionListener(this::numberButtonHandler);
        b0.addActionListener(this::numberButtonHandler);

        alarmPanel1 = new JPanel(new BorderLayout());
        JPanel fill1 = new JPanel();
        fill1.setBackground(Color.RED);
        alarmPanel1.add(fill1, BorderLayout.NORTH);
        alarmPanel1.add(alarmText1, BorderLayout.CENTER);
        alarmPanel1.setBackground(Color.RED);
        alarmPanel1.setVisible(false);

        alarmPanel2 = new JPanel(new BorderLayout());
        JPanel fill2 = new JPanel();
        fill2.setBackground(Color.RED);
        alarmPanel2.add(fill2, BorderLayout.NORTH);
        alarmPanel2.add(alarmText2, BorderLayout.CENTER);
        alarmPanel2.setBackground(Color.RED);
        alarmPanel2.setVisible(false);

        keyboardPanel.add(b1);
        keyboardPanel.add(b2);
        keyboardPanel.add(b3);
        keyboardPanel.add(b4);
        keyboardPanel.add(b5);
        keyboardPanel.add(b6);
        keyboardPanel.add(b7);
        keyboardPanel.add(b8);
        keyboardPanel.add(b9);
        keyboardPanel.add(alarmPanel1);
        keyboardPanel.add(b0);
        keyboardPanel.add(alarmPanel2);

        return keyboardPanel;
    }

    /**
     * Hlavní switch s kódem, vždy je volaná tato akce, pokud se přechází do jiného stavu
     * @param action akce, která způsobila přechod
     */
    public void handleAction(Action action) {
        switch (currentState) {
            case ON:
                if (action == Action.TIMESECOND) {
                    currentState = State.CHECKMOVE;
                    handleAction(null);
                }
                break;
            case OFF:
                if (action == Action.LOCKBUTTON) {
                    currentState = State.CHECKLOCK;
                    handleAction(null);
                }
                break;
            case ALARM:
                if (action == Action.DIGIT) {
                    currentState = State.FIRSTDIGIT;
                    handleAction(null);
                } else if (action == null) {
                    insertedCode = 0;
                    showDisplayMessage("Zadejte kód pro odemčení!");
                    alarm = true;
                    startAlarmTimer();
                    stopTimer();
                }
                break;
            case WARNING:
                if (isDoorOpen() || isWindowOpen()) {
                    showDisplayMessage("Dveře nebo okna nejsou zavřena!");
                } else {
                    showDisplayMessage("Stav: Odkódováno");
                    currentState = State.OFF;
                }
                break;
            case CHECKCODE:
                if (unlockCode == insertedCode) {
                    stopTimer();
                    showDisplayMessage("Stav: Odkódováno");
                    currentState = State.OFF;
                    stopAlarmTimer();
                    time = 0;
                    wrong = 0;
                    alarm = false;
                } else {
                    currentState = State.WRONGCODE;
                    handleAction(null);
                }

                insertedCode = 0;
                break;
            case CHECKLOCK:
                if (isDoorOpen() || isWindowOpen()) {
                    currentState = State.WARNING;
                    handleAction(null);
                } else {
                    currentState = State.WAITINGFORLOCK;
                    showDisplayMessage("Otevřete a zavřete dveře");
                }
                break;
            case CHECKMOVE:
                if (isWindowOpen() || move == 5) {
                    move = 0;
                    currentState = State.ALARM;
                    handleAction(null);
                } else if (isDoorOpen()) {
                    currentState = State.WAITINGFORCODE;
                } else if (isMoving()) {
                    move++;
                    currentState = State.ON;
                } else {
                    move = 0;
                    currentState = State.ON;
                }
                break;
            case WRONGCODE:
                if (action != Action.KEYDISABLEDRETURN) {
                    wrong++;
                }

                if (wrong == 3) {
                    currentState = State.ALARM;
                    handleAction(null);
                    currentState = State.KEYDISABLED;
                    handleAction(null);
                } else {
                    if (alarm) {
                        currentState = State.ALARM;
                    } else {
                        currentState = State.WAITINGFORCODE;
                    }

                    handleAction(null);
                }
                break;
            case FIRSTDIGIT:
                if (!alarm) {
                    showDisplayMessage("Kód: *\nZbývá " + (alarmTimeLimit - time) + " s!");
                } else {
                    showDisplayMessage("Kód: *");
                }

                if (action == Action.TIMESECOND) {
                    checkAlarm(State.FIRSTDIGIT);
                } else if (action == Action.DIGIT) {
                    currentState = State.SECONDDIGIT;
                    handleAction(null);
                }
                break;
            case SECONDDIGIT:
                if (!alarm) {
                    showDisplayMessage("Kód: **\nZbývá " + (alarmTimeLimit - time) + " s!");
                } else {
                    showDisplayMessage("Kód: **");
                }
                if (action == Action.TIMESECOND) {
                    checkAlarm(State.SECONDDIGIT);
                } else if (action == Action.DIGIT) {
                    currentState = State.CHECKCODE;
                    handleAction(null);
                }
                break;
            case WAITINGFORCODE:
                showDisplayMessage("Zadejte kód pro odemčení!\nZbývá " + (alarmTimeLimit - time) + " s!");
                if (action == Action.TIMESECOND) {
                    checkAlarm(State.WAITINGFORCODE);
                    insertedCode = 0;
                } else if (action == Action.DIGIT) {
                    currentState = State.FIRSTDIGIT;
                }

                break;
            case WAITINGFORDOOR:
                if (action == Action.WINDOWOPEN) {
                    currentState = State.WARNING;
                    handleAction(null);
                } else if (action == Action.DOORCLOSE) {
                    showDisplayMessage("Stav: zablokováno");
                    currentState = State.ON;
                    startTimer();
                }
                break;
            case WAITINGFORLOCK:
                if (action == Action.WINDOWOPEN) {
                    currentState = State.WARNING;
                    handleAction(null);
                } else if (action == Action.DOOROPEN) {
                    showDisplayMessage("Zavřete dveře");
                    currentState = State.WAITINGFORDOOR;
                }
                break;
            case KEYDISABLED:
                if (action == null || action == Action.TIMESECOND) {
                    if (keyDisabledTimer >= 60) {
                        timer.stop();
                        keyDisabledTimer = 0;
                        wrong = 0;
                        currentState = State.WRONGCODE;
                        handleAction(Action.KEYDISABLEDRETURN);
                    } else {
                        timer.start();
                        keyDisabledTimer++;
                        showDisplayMessage("Klávesnice zablokována!\nZbývá " + (60 - keyDisabledTimer) + " s");
                    }

                }
            default:
                System.out.println("Unknown state: " + currentState);
        }
        System.out.println("Stav: " + currentState + ", Akce: " + action + ", Dveře: " + isDoorOpen() + ", okna: " + isWindowOpen() + ", pohyb: " + isMoving() + ", počítadlo pohybu: " + move + ", počítadlo doby otevření: " + time + ", počet špatných pokusů: " + wrong);
    }

    /**
     * Kontroluje, jestli uplynul čas pro spuštění alarmu
     * @param state Aktuální stav, do kterého se vrátí po spuštění alarmu
     */
    public void checkAlarm(State state) {
        if (!alarm) {
            time++;
        }

        if (time >= alarmTimeLimit) {
            wrong++;
            currentState = State.ALARM;
            handleAction(null);
            currentState = state;
        }
    }

    /**
     * Event handler, který zpracuje tlačítko zamknutí
     * @param actionEvent event stisknutí
     */
    public void lockButtonHandler(ActionEvent actionEvent) {
        handleAction(Action.LOCKBUTTON);
    }

    /**
     * Event handler, který zpracuje tlačítko dveří
     * @param actionEvent event stisknutí
     */
    public void dvereButtonHandler(ActionEvent actionEvent) {
        if (!doors) {
            bDvere.setText("Zavřít dveře");
            doors = !doors;
            handleAction(Action.DOOROPEN);
        } else {
            bDvere.setText("Otevřít dveře");
            doors = !doors;
            handleAction(Action.DOORCLOSE);
        }

    }

    /**
     * Event handler, který zpracuje tlačítko okna
     * @param actionEvent event stisknutí
     */
    public void oknoButtonHandler(ActionEvent actionEvent) {
        if (!windows) {
            bOkna.setText("Zavřít okna");
            windows = !windows;
            handleAction(Action.WINDOWOPEN);
        } else {
            bOkna.setText("Otevřít okna");
            windows = !windows;
            handleAction(Action.WINDOWCLOSE);
        }

    }

    /**
     * Event handler, který zpracuje tlačítko pohybu
     * @param actionEvent event stisknutí
     */
    public void movementButtonHandler(ActionEvent actionEvent) {
        if (!movement) {
            bPohyb.setText("Pohyb stop");
        } else {
            bPohyb.setText("Pohyb start");
        }

        movement = !movement;
    }

    /**
     * Event handler, který zpracuje tlačítka čísel
     * @param actionEvent event stisknutí
     */
    public void numberButtonHandler(ActionEvent actionEvent) {
        int value = Integer.parseInt(((JButton) actionEvent.getSource()).getText());

        insertedCode *= 10;
        insertedCode += value;

        System.out.println(insertedCode);

        handleAction(Action.DIGIT);
    }

    /**
     * Zajišťuje blikání alarmu
     */
    public void handleAlarmAction() {
        if (alarmPanel1.isVisible()) {
            alarmPanel1.setVisible(false);
            alarmPanel2.setVisible(false);
        } else {
            alarmPanel1.setVisible(true);
            alarmPanel2.setVisible(true);
        }
    }

    /**
     * Vypíše zprávu na displej klávesnice
     * @param message Zpráva
     */
    public void showDisplayMessage(String message) {
        System.out.println("Nastavuji zprávu na: " + message);
        pane.setText(message);
        pane.repaint();
    }

    /**
     * Vrátí, jestli jsou dveře otevřeny
     * @return true = dveře otevřeny, false = dveře zavřeny
     */
    public boolean isDoorOpen() {
        return doors;
    }

    /**
     * Vrátí, jestli je okno otevřeno
     * @return true = okno otevřeno, false = okno zavřeno
     */
    public boolean isWindowOpen() {
        return windows;
    }

    /**
     * Vrátí, jestli je zaznamenán pohyb
     * @return true = pohyb zaznamenán, false = pohyb není zaznamenán
     */
    public boolean isMoving() {
        return movement;
    }

    /**
     * Spustí časovač kontroly
     */
    public void startTimer() {
        timer.start();
    }

    /**
     * Vypne časovač kontroly
     */
    public void stopTimer() {
        timer.stop();
    }

    /**
     * Spustí časovač blikání alarmu
     */
    public void startAlarmTimer() {
        alarmBlinkTimer.start();
    }

    /**
     * Vypne časovač blikání alarmu
     */
    public void stopAlarmTimer() {
        alarmBlinkTimer.stop();
        alarmPanel1.setVisible(false);
        alarmPanel2.setVisible(false);
    }
}

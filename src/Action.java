/**
 * Enum, který obsahuje akce, kterými se přesouvá mezi stavy
 */
public enum Action {
    LOCKBUTTON, DOOROPEN, DOORCLOSE, WINDOWOPEN, WINDOWCLOSE, TIMESECOND, DIGIT, KEYDISABLEDRETURN
}

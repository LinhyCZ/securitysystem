/**
 * Hlavní třída programu, zajišťuje spuštění
 */
public class main {
    /**
     * Vstupní bod programu, spustí klávesnici
     * @param args argumenty z příkazové řádky
     */
    public static void main(String[] args) {
        KeyboardFrame frame = new KeyboardFrame();

        frame.repaint();
    }
}
